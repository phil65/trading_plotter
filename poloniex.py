# -*- coding: utf-8 -*-
"""
@author: Philipp
"""

import urllib.parse
import time
import hmac
import hashlib
import requests
import functools
import ratelimit

# import requests_cache

# requests_cache.install_cache('poloniex')


def createTimeStamp(datestr, format="%Y-%m-%d %H:%M:%S"):
    return time.mktime(time.strptime(datestr, format))


def urlencode(dct):
    return urllib.parse.urlencode(dct)


class API(object):

    NAME = "Poloniex"

    def __init__(self, *args, **kwargs):
        self.APIKey = kwargs.get("APIKey")
        self.Secret = kwargs.get("Secret")
        self.chart_intervals = [5, 15, 30, 120, 240, 1440]

    def post_process(self, data):
        # Add timestamps if there isnt one but is a datetime
        if 'return' not in data or not isinstance(data['return'], list):
            return data
        for item in data['return']:
            if(isinstance(item, dict)):
                if('datetime' in item and 'timestamp' not in item):
                    item['timestamp'] = float(createTimeStamp(item['datetime']))
        return data

    def get_json(self, url):
        ret = requests.get(url)
        data = ret.json()
        if isinstance(data, dict) and data.get("error"):
            print(url)
            print(data.get("error"))
        return data

    @ratelimit.rate_limited(period=3, every=1.0)
    def api_query(self, command, req=None):
        req = req if req else {}
        if command in {"returnTicker", "return24Volume"}:
            return self.get_json('https://poloniex.com/public?command=' + command)
        elif command in {"returnOrderBook", "returnChartData"}:
            req["command"] = command
            return self.get_json('http://poloniex.com/public?%s' % urlencode(req))
        elif(command == "returnMarketTradeHistory"):
            params = {"command": "returnTradeHistory",
                      "currencyPair": req['currencyPair']}
            return self.get_json('http://poloniex.com/public?%s' % urlencode(params))
        req['command'] = command
        req['nonce'] = int(time.time() * 1000)
        post_data = urlencode(req).encode()

        sign = hmac.new(key=self.Secret.encode(),
                        msg=post_data,
                        digestmod=hashlib.sha512)
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
            'Sign': sign.hexdigest(),
            'Key': self.APIKey
        }
        req = requests.post(url='https://poloniex.com/tradingApi', data=req, headers=headers)
        data = req.json()
        if data.get("error"):
            print("req", data)
            print("headers", headers)
            print(data.get("error"))
        return self.post_process(data)

    def returnTicker(self):
        return self.api_query("returnTicker")

    def return24Volume(self):
        return self.api_query("return24Volume")

    def returnOrderBook(self, currencyPair):
        return self.api_query("returnOrderBook", {'currencyPair': currencyPair})

    def returnMarketTradeHistory(self, currencyPair):
        return self.api_query("returnMarketTradeHistory", {'currencyPair': currencyPair})

    @functools.lru_cache(maxsize=32)
    def return_trading_pairs(self):
        ticker_data = self.api_query("returnTicker")
        data = list(ticker_data.keys())
        data.sort()
        return data

    # Returns chart data for a given market, specified by the "currencyPair" POST parameter
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # period        Time period of one bar
    # start         Start time as Unix Timestamp
    # end           End time as Unix Timestamp
    def return_chart_data(self, pair, period, start, end=None):
        assert period in self.chart_intervals
        period = period * 60
        if not end:
            end = time.time()
        req = {"currencyPair": pair,
               "period": period,
               "start": start,
               "end": end}
        print(req)
        ret = self.api_query('returnChartData', req)
        return ret

    # Returns all of your balances.
    # Outputs:
    # {"BTC":"0.59098578","LTC":"3.31117268", ... }
    def returnBalances(self):
        return self.api_query('returnBalances')

    # Returns all of your deposit addresses.
    # Outputs:
    # {"BTC":"abc...","LTC":"def...", ... }
    def returnDepositAddresses(self):
        return self.api_query('returnDepositAddresses')

    # Returns your open orders for a given market, specified by the "currencyPair" POST parameter, e.g. "BTC_XCP"
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # Outputs:
    # orderNumber   The order number
    # type          sell or buy
    # rate          Price the order is selling or buying at
    # Amount        Quantity of order
    # total         Total value of order (price * quantity)
    def returnOpenOrders(self, currencyPair):
        return self.api_query('returnOpenOrders', {"currencyPair": currencyPair})

    # Returns your trade history for a given market, specified by the "currencyPair" POST parameter
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # Outputs:
    # date          Date in the form: "2014-02-19 03:44:59"
    # rate          Price the order is selling or buying at
    # amount        Quantity of order
    # total         Total value of order (price * quantity)
    # type          sell or buy
    def returnTradeHistory(self, currencyPair):
        return self.api_query('returnTradeHistory', {"currencyPair": currencyPair})

    # Places a buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is buying at
    # amount        Amount of coins to buy
    # Outputs:
    # orderNumber   The order number
    def buy(self, currencyPair, rate, amount):
        return self.api_query('buy', {"currencyPair": currencyPair, "rate": rate, "amount": amount})

    # Places a buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is buying at
    # amount        Amount of coins to buy
    # Outputs:
    # orderNumber   The order number
    def marginBuy(self, currencyPair, rate, amount):
        return self.api_query('marginBuy', {"currencyPair": currencyPair, "rate": rate, "amount": amount})

    # Places a sell order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is selling at
    # amount        Amount of coins to sell
    # Outputs:
    # orderNumber   The order number
    def sell(self, currencyPair, rate, amount):
        return self.api_query('sell', {"currencyPair": currencyPair, "rate": rate, "amount": amount})

    # Places a sell order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is selling at
    # amount        Amount of coins to sell
    # Outputs:
    # orderNumber   The order number
    def marginSell(self, currencyPair, rate, amount):
        return self.api_query('marginSell', {"currencyPair": currencyPair, "rate": rate, "amount": amount})

    # Cancels an order you have placed in a given market. Required POST parameters are "currencyPair" and "orderNumber".
    # Inputs:
    # currencyPair  The curreny pair
    # orderNumber   The order number to cancel
    # Outputs:
    # succes        1 or 0
    def cancel(self, currencyPair, orderNumber):
        return self.api_query('cancelOrder', {"currencyPair": currencyPair, "orderNumber": orderNumber})

    # Immediately places a withdrawal for a given currency, with no email confirmation. In order to use this method, the withdrawal privilege must be enabled for your API key. Required POST parameters are "currency", "amount", and "address". Sample output: {"response":"Withdrew 2398 NXT."}
    # Inputs:
    # currency      The currency to withdraw
    # amount        The amount of this coin to withdraw
    # address       The withdrawal address
    # Outputs:
    # response      Text containing message about the withdrawal
    def withdraw(self, currency, amount, address):
        return self.api_query('withdraw', {"currency": currency, "amount": amount, "address": address})

    def save(self, amount):
        return self.withdraw("BTC", amount, "1BaJxi5bj9kEKqGzaTW9KdqtEHWbfEwi1X")
