# -*- coding: utf-8 -*-
"""
@author: Philipp Temminghoff
"""

import json
import os
import urllib
import sys
import time
import datetime

import pandas
from stockstats import StockDataFrame
import numpy as np
# import pygame


def get_script_path():
    try:
        print(os.path.dirname(os.path.abspath(__file__)))
        return os.path.dirname(os.path.abspath(__file__))
    except Exception:
        return os.path.dirname(os.path.realpath(sys.argv[0]))


def dump_dict(dct):
    return json.dumps(dct,
                      sort_keys=True,
                      indent=4,
                      separators=(',', ': '))


def to_timestamp(time_string):
    return int(time.mktime(datetime.datetime.strptime(time_string, "%d/%m/%Y %H:%M").timetuple()))


def pp(string):
    """
    prettyprint json
    """
    print(dump_dict(string))


def urlencode(dct):
    return urllib.parse.urlencode(dct)


def play_sound():
    pygame.mixer.music.play()


def read_credentials(filename):
    path = os.path.join(get_script_path(), f'{filename}.txt')
    if os.path.exists(path):
        with open(path, 'r') as f:
            return [line.strip() for line in f if line]
    else:
        print(f"{filename}.txt does not exist")
        return []


def bbands(df, window_size=10, num_of_std=3):
    rolling_mean = df["close"].rolling(window=window_size).mean()
    rolling_std = df["close"].rolling(window=window_size).std()
    upper_band = rolling_mean + (rolling_std * num_of_std)
    lower_band = rolling_mean - (rolling_std * num_of_std)
    df["close_20_sma"] = rolling_mean
    df["boll_ub"] = upper_band
    df["boll_lb"] = lower_band
    return df


def movingaverage(interval, window_size=10):
    window = np.ones(int(window_size)) / float(window_size)
    return np.convolve(interval, window, 'same')


def prepare_datatable(data):
    df = pandas.DataFrame.from_records(data)
    df = df.ix[1:]
    df["timestamp"] = df.date.copy()
    df.date = pandas.to_datetime(df.date, unit='s')
    df.set_index("date", inplace=True)
    df["time"] = df.index.copy()
    df = bbands(df)
    # df = StockDataFrame.retype(df)
    # df["boll_ub"]
    # df['cr']
    return df


def prepare_timestring(minutes):
    if minutes >= 1440:
        return "%iD" % (minutes / (60 * 24))
    if minutes >= 60:
        return "%ih" % (minutes / 60)
    return "%im" % (minutes)

# pygame.mixer.init()
# pygame.mixer.music.load(os.path.join(get_script_path(), "sound.wav"))
