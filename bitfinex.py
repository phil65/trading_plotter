# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 22:04:39 2017

@author: Philipp Temminghoff
"""

import functools
import requests
import utils
import time


class API(object):

    NAME = "Bitfinex"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chart_intervals = [1, 5, 15, 30, 60, 180, 360, 720, 1440, 10080, 20160]

    @functools.lru_cache(maxsize=32)
    def return_trading_pairs(self):
        url = "https://api.bitfinex.com/v1/symbols"
        response = requests.request("GET", url)
        return response.json()

    @functools.lru_cache(maxsize=32)
    def return_chart_data(self, pair, period, since, end=None):
        timeframe = utils.prepare_timestring(period)
        params = {'start': since * 1000, 'end': time.time() * 1000}
        url = f"https://api.bitfinex.com/v2/candles/trade:{timeframe}:t{pair.upper()}/hist"
        r = requests.get(url, params=params)
        dicts = []
        for item in r.json():
            item = {"date": item[0] / 1000,
                    "open": item[1],
                    "close": item[2],
                    "high": item[3],
                    "low": item[4],
                    "volume": item[5]}
            dicts.append(item)
        return dicts
