# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 22:04:39 2017

@author: Philipp Temminghoff
"""

import functools
import requests
import time
import utils

class API(object):

    NAME = "Cryptopia"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chart_intervals = [60, 120, 240, 720, 1440]
        self.trading_pairs = []
        self.trading_pair_map = {}

    @functools.lru_cache(maxsize=32)
    def return_trading_pairs(self):
        url = "https://www.cryptopia.co.nz/api/GetTradePairs"
        response = requests.request("GET", url).json()
        self.trading_pairs = [i["Label"] for i in response["Data"]]
        self.trading_pair_map = {i["Label"]: i["Id"] for i in response["Data"]}
        return self.trading_pairs

    @functools.lru_cache(maxsize=32)
    def return_chart_data(self, pair, period, since, end=None):
        pair_id = self.trading_pair_map[pair]
        params = {'tradePairId': pair_id,
                  'dataRange': 4,
                  'dataGroup': period}
        print(params)
        url = f"https://www.cryptopia.co.nz/Exchange/GetTradePairChart?{utils.urlencode(params)}"
        r = requests.get(url, params=params)
        ret = r.json()
        dicts = []
        for candle, volume in zip(ret["Candle"], ret["Volume"]):
            item = {"date": candle[0] / 1000,
                    "open": candle[1],
                    "close": candle[4],
                    "high": candle[2],
                    "low": candle[3],
                    "volume": volume["basev"]}
            dicts.append(item)
        print(len(dicts))
        return dicts
