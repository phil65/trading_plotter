# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 22:04:39 2017

@author: Philipp Temminghoff
"""

import krakenex
import functools
import utils


class API(krakenex.API):

    NAME = "Kraken"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chart_intervals = [5, 15, 30, 60, 240, 1440, 10080, 21600]

    @functools.lru_cache(maxsize=32)
    def return_trading_pairs(self):
        res = self.query_public('AssetPairs')
        result = list(res["result"].keys())
        return result

    @functools.lru_cache(maxsize=32)
    def return_chart_data(self, pair, period, since, end=None):
        assert period in self.chart_intervals
        req = {'pair': pair,
               'since': since,
               "interval": period}
        ret = self.query_public('OHLC', req=req)
        dicts = []
        if "result" not in ret:
            print("Could not fetch data from kraken")
            print(ret)
            return []
        for item in ret["result"][pair]:
            item = {"date": item[0],
                    "open": float(item[1]),
                    "high": float(item[2]),
                    "low": float(item[3]),
                    "close": float(item[4]),
                    "volume": float(item[6])}
            dicts.append(item)
        return dicts


if __name__ == '__main__':
    api = API(*utils.read_credentials("kraken"))
