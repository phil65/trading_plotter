# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 04:04:19 2017

@author: Philipp Temminghoff
"""

import utils
import plotly

# import plotly.graph_objs as go

# plotly.offline.init_notebook_mode(connected=True)


class CandleStickGraph(object):

    def __init__(self, *args, **kwargs):
        self.title = kwargs.get("title")
        self.rangeselector = {
            "visible": True,
            "x": 0,
            "y": 0.9,
            "bgcolor": 'rgba(150, 200, 250, 0.4)',
            "font": {"size": 13},
            "buttons": list([
                {"count": 1,
                 "label": 'reset',
                 "step": 'all'},
                {"count": 1,
                 "label": '1yr',
                 "step": 'year',
                 "stepmode": 'todate'},
                {"count": 3,
                 "label": '3 mo',
                 "step": 'month',
                 "stepmode": 'todate'},
                {"count": 1,
                 "label": '1 mo',
                 "step": 'month',
                 "stepmode": 'todate'},
                {"step": 'all'}
            ])}

        self.layout = {
            'title': self.title,
            'xaxis': {"rangeselector": self.rangeselector,
                      "rangeslider": {},
                      "type": "date"},
            'yaxis': {'title': 'Value (BTC)',
                      "domain": [0, 0.2],
                      "showticklabels": False},
            'yaxis2': {"domain": [0.2, 0.8]},
            "plot_bgcolor": 'rgb(250, 250, 250)',
            "shapes": [],
            "annotations": [],
            "legend": {"orientation": "h",
                       "y": 0.9,
                       "x": 0.3,
                       "yanchor": "bottom"},
            "margin": {"t": 40, "b": 40,
                       "r": 40, "l": 40}
        }
        self.INCREASING_COLOR = '#66CC00'
        self.DECREASING_COLOR = '#FF2222'

    def set_graph(self, df):

        mv_y = utils.movingaverage(df.close)
        mv_x = list(df.index)

        # Clip the ends
        mv_x = mv_x[5:-5]
        mv_y = mv_y[5:-5]

        # candlestick_data = go.Candlestick(x=df.index,
        #                        open=df.open,
        #                        high=df.high,
        #                        low=df.low,
        #                        close=df.close)

        colors = []

        for i in range(len(df.close)):
            if i != 0:
                if df.close.iloc[i] > df.close.iloc[i - 1]:
                    colors.append(self.INCREASING_COLOR)
                else:
                    colors.append(self.DECREASING_COLOR)
            else:
                colors.append(self.DECREASING_COLOR)

        bb_avg, bb_upper, bb_lower = utils.bbands(df.close)

        candlestick_data = {
            "type": 'candlestick',
            "open": df.open,
            "high": df.high,
            "low": df.low,
            "close": df.close,
            "x": df.index,
            "yaxis": 'y2',
            "name": 'GS',
            "increasing": {"line": {"color": self.INCREASING_COLOR}},
            "decreasing": {"line": {"color": self.DECREASING_COLOR}},
        }

        moving_average_data = {"x": mv_x,
                               "y": mv_y,
                               "type": 'scatter',
                               "mode": 'lines',
                               "line": {"width": 1},
                               "marker": {"color": '#E377C2'},
                               "yaxis": 'y2',
                               "name": 'Moving Average'}

        upper_bb_data = {"x": df.index,
                         "y": bb_upper,
                         "type": 'scatter',
                         "yaxis": 'y2',
                         "line": {"width": 1},
                         "marker": {"color": '#ccc'},
                         "hoverinfo": 'none',
                         "legendgroup": 'Bollinger Bands',
                         "name": 'Bollinger Bands'}

        lower_bb_data = {"x": df.index,
                         "y": bb_lower,
                         "type": 'scatter',
                         "yaxis": 'y2',
                         "line": {"width": 1},
                         "marker": {"color": '#ccc'},
                         "hoverinfo": 'none',
                         "legendgroup": 'Bollinger Bands',
                         "showlegend": False}

        volume_data = {"x": df.index,
                       "y": df.volume,
                       "marker": {"color": colors},
                       "type": 'bar',
                       "yaxis": 'y',
                       "name": 'Volume'}

        self.fig = {"data": [candlestick_data,
                             moving_average_data,
                             upper_bb_data,
                             lower_bb_data,
                             volume_data],
                    "layout": self.layout}

    def add_bump_marker(self, row, text=""):
        shape = {'x0': row[0], 'x1': row[0],
                 'y0': 0,
                 'y1': 1,
                 'xref': 'x',
                 'yref': 'paper',
                 'line': {'color': 'rgb(30,30,30)',
                          'width': 1}}
        annotation = {'x': row[0],
                      'y': (0.05 * len(self.layout["shapes"])) % 1,
                      'xref': 'x',
                      'yref': 'paper',
                      'showarrow': False,
                      'xanchor': 'left',
                      'text': "%.3f" % text if isinstance(text, float) else ""}
        self.layout["shapes"].append(shape)
        self.layout["annotations"].append(annotation)

    def show_graph(self):
        plotly.offline.plot(self.fig,
                            filename=f'{self.title}.html',
                            validate=False)
