# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 04:35:28 2017

@author: Philipp Temminghoff
"""

from math import pi
from bokeh import plotting
from bokeh import layouts
from bokeh import models
from bokeh import io
import kraken
import poloniex
import bitfinex
import cryptopia
import time

import utils


def find_dumps(df):
    return df[(df.boll_lb > df.low)]


class CandleStickGraph(object):

    TITLE = "Chart browser"
    zoom_type = "width"

    def __init__(self, *args, **kwargs):
        self.start_date = kwargs.get("start_date")
        self.end_date = kwargs.get("end_date")

        self.providers = {"Kraken": kraken.API(),
                          "Cryptopia": cryptopia.API(),
                          "Poloniex": poloniex.API(*utils.read_credentials("poloniex")),
                          "Bitfinex": bitfinex.API()}

        self.provider = kwargs.get("provider", self.providers["Kraken"])
        self.interval = kwargs.get("interval", self.provider.chart_intervals[1])
        self.trading_pairs = self.provider.return_trading_pairs()
        self.trading_pair = kwargs.get("trading_pair", self.trading_pairs[0])
        self.plot = plotting.Figure(x_axis_type="datetime",
                                    x_range=models.DataRange1d(bounds='auto'),
                                    y_range=models.DataRange1d(bounds='auto'),
                                    output_backend="webgl",
                                    tools="undo,redo,reset,save",
                                    plot_width=1000,
                                    title=self.trading_pair,
                                    y_axis_location="right")
        self.plot.xaxis.major_label_orientation = pi / 4
        self.plot.grid.grid_line_alpha = 0.3
        self.plot.yaxis[0].formatter.use_scientific = False
        legend = models.Legend(items=[],
                               location=(0, -30))
        legend.click_policy = "hide"
        self.plot.add_layout(legend, 'right')
        hover = models.HoverTool(tooltips=[("time", "@time{%D %H:%M}"),
                                           ("open", "@open{0.0[0000000]}"),
                                           ("close", "@close{0.0[0000000]}"),
                                           ("low", "@low{0.0[0000000]}"),
                                           ("high", "@high{0.0[0000000]}"),
                                           ("volume", "@volume{0.0}")],
                                 formatters={"time": "datetime"},
                                 mode='vline',
                                 names=["tooltip"])
        boxzoom_tool = models.tools.BoxZoomTool(dimensions=self.zoom_type)
        pan_tool = models.tools.PanTool(dimensions=self.zoom_type)
        wheel_tool = models.tools.WheelZoomTool(dimensions=self.zoom_type)
        self.plot.add_tools(boxzoom_tool, pan_tool, wheel_tool, hover)
        self.plot2 = plotting.Figure(plot_height=100,
                                     tools=[],
                                     output_backend="webgl",
                                     x_axis_type="datetime",
                                     x_range=self.plot.x_range,
                                     y_axis_location="right")
        self.plot2.yaxis[0].formatter.use_scientific = False

        df = self.fetch_data()
        self.df = df
        self.source = models.ColumnDataSource(df)
        self.inc = models.ColumnDataSource(df[df.close > df.open])
        self.dec = models.ColumnDataSource(df[df.open > df.close])
        self.dumps = models.ColumnDataSource(find_dumps(df))
        self.set_graph()

    def set_graph(self):
        self.plot.line("time", "close", source=self.source, name="tooltip", line_color="white")
        self.plot.segment(x0="time", y0="high", x1="time", y1="low", source=self.source, color="black")
        self.plot.vbar("time", "interval", "open", "close", source=self.inc, fill_color="#D5E1DD", line_alpha=0.0)
        self.plot.vbar("time", "interval", "open", "close", source=self.dec, fill_color="#F2583E", line_alpha=0.0)
        self.plot.circle("time", "low", source=self.dumps, size=10, color="navy", alpha=0.5)
        self.plot.line("time", "boll_ub", source=self.source, legend="Bollinger Bands", name="boll_up")
        self.plot.line("time", "boll_lb", source=self.source, legend="Bollinger Bands", name="boll_lp")
        self.plot.line("time", "close_20_sma", source=self.source, legend="SMA20", line_color="red")
        self.plot2.vbar("time", "interval", "volume", 0, source=self.source)

    def add_bump_marker(self, row, text=""):
        loc = time.mktime(row[0].timetuple()) * 1000
        hline = models.Span(location=loc,
                            dimension='height',
                            line_color='black',
                            line_width=1,
                            name="marker")
        self.plot.add_layout(hline)
        citation = models.Label(x=loc, y=0, x_units='data', y_units='screen',
                                text="%.3f" % text if isinstance(text, float) else "",
                                render_mode='css',
                                border_line_color='black',
                                border_line_alpha=1.0,
                                text_font_size="8pt",
                                background_fill_color='white',
                                background_fill_alpha=1.0)
        self.plot.add_layout(citation)

    def show_graph(self):
        # plotting.output_file("candlestick.html", title=self.TITLE)
        pl = layouts.gridplot([[self.plot], [self.plot2]],
                              toolbar_location="left",
                              toolbar_options={"logo": None},
                              # responsive=True,
                              plot_width=1000)
        provider_select = models.widgets.Select(title="Exchange:",
                                                value="Kraken",
                                                options=list(self.providers.keys()))
        self.pair_select = models.widgets.Select(title="Trading pair:",
                                                 value="foo",
                                                 options=self.trading_pairs)
        intervals = [utils.prepare_timestring(i) for i in self.provider.chart_intervals]
        self.interval_select = models.widgets.RadioButtonGroup(labels=intervals, active=0)
        widgets = layouts.row([provider_select, self.pair_select, self.interval_select])
        self.interval_select.on_click(self.set_interval)
        self.pair_select.on_change('value', self.set_coin_pair)
        provider_select.on_change('value', self.set_provider)
        self.slider_boll_window = models.widgets.Slider(start=5, end=50, step=1, title="Window size")
        self.slider_boll_window.on_change('value', self.update_bbands)
        self.slider_boll_num_std = models.widgets.Slider(start=1, end=10, title="Num of Std")
        self.slider_boll_num_std.on_change('value', self.update_bbands)
        doc = io.curdoc()
        boll_widgets = layouts.row(self.slider_boll_window, self.slider_boll_num_std)
        root = layouts.column(widgets, pl, boll_widgets)
        doc.add_root(root)
        doc.title = self.TITLE

        # plotting.show(layouts.column(widgets, pl))  # open a browser

    def fetch_data(self):
        data = self.provider.return_chart_data(self.trading_pair,
                                               self.interval,
                                               self.start_date,
                                               self.end_date)
        if not data:
            print("could not get data from", self.provider.NAME)
            return None
        df = utils.prepare_datatable(data)
        df["interval"] = self.interval * 60 * 1000
        df.sort_index(inplace=True)
        return df

    def set_interval(self, new):
        self.interval = self.provider.chart_intervals[new]
        self.update()

    def set_coin_pair(self, att, old, new):
        self.trading_pair = new
        self.update()

    def set_provider(self, att, old, new):
        self.provider = self.providers[new]
        pairs = self.provider.return_trading_pairs()
        self.pair_select.options = pairs
        self.trading_pair = pairs[0]
        self.interval = self.provider.chart_intervals[1]
        intervals = [utils.prepare_timestring(i) for i in self.provider.chart_intervals]
        self.interval_select.labels = intervals
        self.update()

    def set_dumps(self):
        df = self.df
        self.dumps.data = self.source.from_df(find_dumps(df))

    def update_bbands(self, att, old, new):
        window_size = self.slider_boll_window.value
        num_of_std = self.slider_boll_num_std.value
        df = utils.bbands(self.df, window_size=int(window_size), num_of_std=num_of_std)
        self.source.data = self.source.from_df(df)
        self.set_dumps()

    def update(self):
        df = self.fetch_data()
        if df is not None:
            self.df = df
            self.source.data = self.source.from_df(df)
            self.inc.data = self.source.from_df(df[df.close > df.open])
            self.dec.data = self.source.from_df(df[df.open > df.close])
            self.plot.title.text = self.trading_pair
            self.set_dumps()
