#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 22 01:43:03 2017

@author: Philipp Temminghoff
"""

import utils
import bokeh_graph

start_date = utils.to_timestamp("01/02/2017 00:00")
end_date = None

graph = bokeh_graph.CandleStickGraph(start_date=utils.to_timestamp("01/06/2017 00:00"),
                                     end_date=None)

graph.show_graph()
